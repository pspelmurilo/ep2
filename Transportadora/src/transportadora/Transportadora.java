
package transportadora;

import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JPanel;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;



public class Transportadora extends javax.swing.JFrame {
    
    
    public Transportadora() {
       initComponents();
       switchpanel(Menu);
       inicialistas();
       this.setPreco_gasolina(4.449f);
       this.setPreco_alcool(3.499f);
       this.setPreco_diesel(3.869f);
       this.setMargem_lucro(0f);
       try {
            le_arquivo();
        } catch (IOException ex) {
            Logger.getLogger(Transportadora.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLayeredPane1 = new javax.swing.JLayeredPane();
        Menu = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        Gerenciador_de_Veiculos = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton9 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        jButton19 = new javax.swing.JButton();
        jButton20 = new javax.swing.JButton();
        jButton21 = new javax.swing.JButton();
        jButton22 = new javax.swing.JButton();
        Alterar_Especificacoes = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        gasolina = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        preco_gasolina_atual = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        alcool = new javax.swing.JTextField();
        preco_alcool_atual = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        preco_diesel_atual = new javax.swing.JLabel();
        diesel = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        margem_atual = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        lucro = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        erro = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        Frete = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        lucrotot = new javax.swing.JLabel();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton15 = new javax.swing.JButton();
        Calcula_Frete = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        peso_texto = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        distancia_texto = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        dias_texto = new javax.swing.JTextField();
        horas_texto = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jButton16 = new javax.swing.JButton();
        jButton17 = new javax.swing.JButton();
        erro2 = new javax.swing.JLabel();
        Opcoes_Frete = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        tipo_menor_custo = new javax.swing.JLabel();
        tempo_menor_custo = new javax.swing.JLabel();
        custo_menor_custo = new javax.swing.JLabel();
        frete_menor_custo = new javax.swing.JLabel();
        lucro_menor_custo = new javax.swing.JLabel();
        jButton18 = new javax.swing.JButton();
        jLabel35 = new javax.swing.JLabel();
        tipo_menor_tempo = new javax.swing.JLabel();
        tempo_menor_tempo = new javax.swing.JLabel();
        custo_menor_tempo = new javax.swing.JLabel();
        frete_menor_tempo = new javax.swing.JLabel();
        lucro_menor_tempo = new javax.swing.JLabel();
        jButton23 = new javax.swing.JButton();
        jLabel41 = new javax.swing.JLabel();
        frete_maior_lucro = new javax.swing.JLabel();
        tipo_maior_lucro = new javax.swing.JLabel();
        tempo_maior_lucro = new javax.swing.JLabel();
        custo_maior_lucro = new javax.swing.JLabel();
        lucro_maior_lucro = new javax.swing.JLabel();
        jButton24 = new javax.swing.JButton();
        jLabel47 = new javax.swing.JLabel();
        jButton25 = new javax.swing.JButton();
        comb_maior_lucro = new javax.swing.JLabel();
        comb_menor_custo = new javax.swing.JLabel();
        comb_menor_tempo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(540, 530));

        Menu.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        Menu.setPreferredSize(new java.awt.Dimension(810, 505));

        jButton1.setText("Gerenciar Veículos");
        jButton1.setPreferredSize(new java.awt.Dimension(140, 30));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 20)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Transportadora");
        jLabel1.setMaximumSize(new java.awt.Dimension(150, 24));
        jLabel1.setMinimumSize(new java.awt.Dimension(150, 24));
        jLabel1.setPreferredSize(new java.awt.Dimension(150, 24));

        jButton2.setText("Calcular Entrega");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Alterar Especificações");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Salvar e Sair");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText("Fechar sem Salvar");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout MenuLayout = new javax.swing.GroupLayout(Menu);
        Menu.setLayout(MenuLayout);
        MenuLayout.setHorizontalGroup(
            MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenuLayout.createSequentialGroup()
                .addGroup(MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(MenuLayout.createSequentialGroup()
                        .addGap(271, 271, 271)
                        .addGroup(MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(MenuLayout.createSequentialGroup()
                        .addGap(261, 261, 261)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(336, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, MenuLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        MenuLayout.setVerticalGroup(
            MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenuLayout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        Gerenciador_de_Veiculos.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Gerenciador_de_Veiculos.setPreferredSize(new java.awt.Dimension(810, 505));

        jLabel2.setFont(new java.awt.Font("Ubuntu", 1, 17)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Gerenciador de Veiculo");

        jTable1.setBackground(new java.awt.Color(242, 241, 240));
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Moto", "0", "0", "50 kg", "43 Km/L - Alcool, 50 Km/L - Gasolina"},
                {"Carro", "0", "0", "360 kg", "12 Km/L - Álcool, 14Km/L - Gasolina"},
                {"Van", "0", "0", "3,5 ton", "10 Km/L - Diesel"},
                {"Carreta", "0", "0", "30 ton", "8 Km/L - Diesel"}
            },
            new String [] {
                "Tipo", "Qtde. Total", "Disponíveis", "Carga Máx", "Rendimento"
            }
        ));
        jTable1.setRowHeight(45);
        jTable1.setSelectionForeground(new java.awt.Color(254, 254, 254));
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(80);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(80);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(80);
            jTable1.getColumnModel().getColumn(1).setMinWidth(100);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(1).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(2).setMinWidth(120);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(120);
            jTable1.getColumnModel().getColumn(2).setMaxWidth(120);
            jTable1.getColumnModel().getColumn(3).setMinWidth(100);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(3).setMaxWidth(100);
        }

        jButton9.setText("+");
        jButton9.setMaximumSize(new java.awt.Dimension(30, 30));
        jButton9.setMinimumSize(new java.awt.Dimension(30, 30));
        jButton9.setPreferredSize(new java.awt.Dimension(30, 30));
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton7.setText("Remover Todos Veículos");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton8.setText("Voltar");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton12.setText("-");
        jButton12.setMaximumSize(new java.awt.Dimension(30, 30));
        jButton12.setMinimumSize(new java.awt.Dimension(30, 30));
        jButton12.setPreferredSize(new java.awt.Dimension(30, 30));
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        jButton13.setText("-");
        jButton13.setMaximumSize(new java.awt.Dimension(30, 30));
        jButton13.setMinimumSize(new java.awt.Dimension(30, 30));
        jButton13.setPreferredSize(new java.awt.Dimension(30, 30));
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });

        jButton14.setText("-");
        jButton14.setMaximumSize(new java.awt.Dimension(30, 30));
        jButton14.setMinimumSize(new java.awt.Dimension(30, 30));
        jButton14.setPreferredSize(new java.awt.Dimension(30, 30));
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });

        jButton19.setText("-");
        jButton19.setMaximumSize(new java.awt.Dimension(30, 30));
        jButton19.setMinimumSize(new java.awt.Dimension(30, 30));
        jButton19.setPreferredSize(new java.awt.Dimension(30, 30));
        jButton19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton19ActionPerformed(evt);
            }
        });

        jButton20.setText("+");
        jButton20.setMaximumSize(new java.awt.Dimension(30, 30));
        jButton20.setMinimumSize(new java.awt.Dimension(30, 30));
        jButton20.setPreferredSize(new java.awt.Dimension(30, 30));
        jButton20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton20ActionPerformed(evt);
            }
        });

        jButton21.setText("+");
        jButton21.setMaximumSize(new java.awt.Dimension(30, 30));
        jButton21.setMinimumSize(new java.awt.Dimension(30, 30));
        jButton21.setPreferredSize(new java.awt.Dimension(30, 30));
        jButton21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton21ActionPerformed(evt);
            }
        });

        jButton22.setText("+");
        jButton22.setMaximumSize(new java.awt.Dimension(30, 30));
        jButton22.setMinimumSize(new java.awt.Dimension(30, 30));
        jButton22.setPreferredSize(new java.awt.Dimension(30, 30));
        jButton22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton22ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout Gerenciador_de_VeiculosLayout = new javax.swing.GroupLayout(Gerenciador_de_Veiculos);
        Gerenciador_de_Veiculos.setLayout(Gerenciador_de_VeiculosLayout);
        Gerenciador_de_VeiculosLayout.setHorizontalGroup(
            Gerenciador_de_VeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Gerenciador_de_VeiculosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Gerenciador_de_VeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(Gerenciador_de_VeiculosLayout.createSequentialGroup()
                        .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(Gerenciador_de_VeiculosLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 649, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(Gerenciador_de_VeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(Gerenciador_de_VeiculosLayout.createSequentialGroup()
                                .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(Gerenciador_de_VeiculosLayout.createSequentialGroup()
                                .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(Gerenciador_de_VeiculosLayout.createSequentialGroup()
                                .addComponent(jButton22, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(Gerenciador_de_VeiculosLayout.createSequentialGroup()
                                .addComponent(jButton20, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton19, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(Gerenciador_de_VeiculosLayout.createSequentialGroup()
                .addGap(264, 264, 264)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        Gerenciador_de_VeiculosLayout.setVerticalGroup(
            Gerenciador_de_VeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Gerenciador_de_VeiculosLayout.createSequentialGroup()
                .addGroup(Gerenciador_de_VeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(Gerenciador_de_VeiculosLayout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(55, 55, 55)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(Gerenciador_de_VeiculosLayout.createSequentialGroup()
                        .addGap(163, 163, 163)
                        .addGroup(Gerenciador_de_VeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(Gerenciador_de_VeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(Gerenciador_de_VeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(Gerenciador_de_VeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(36, 36, 36)
                .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );

        Alterar_Especificacoes.setMaximumSize(new java.awt.Dimension(810, 505));
        Alterar_Especificacoes.setPreferredSize(new java.awt.Dimension(788, 505));

        jLabel3.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Alterar Especificações");
        jLabel3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel4.setText("Preço Gasolina:");

        gasolina.setText("4.449");
        gasolina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gasolinaActionPerformed(evt);
            }
        });

        jLabel5.setText("Alterar Preço:");

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel6.setText("R$");

        preco_gasolina_atual.setText("  4.449      R$");

        jLabel8.setText("Preço Álcool:");

        jLabel9.setText("Alterar Preço:");

        alcool.setText("3.499");
        alcool.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                alcoolActionPerformed(evt);
            }
        });

        preco_alcool_atual.setText("  3.499      R$");

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel11.setText("R$");

        jLabel12.setText("Preço Diesel:");

        jLabel13.setText("Alterar Preço:");

        preco_diesel_atual.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        preco_diesel_atual.setText("3.869         R$");

        diesel.setText("3.869");
        diesel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dieselActionPerformed(evt);
            }
        });

        jLabel15.setText("R$");

        jLabel16.setText("Margem de Lucro:");

        margem_atual.setText("0 %");

        jLabel18.setText("Alterar Margem:");

        lucro.setText("0");
        lucro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lucroActionPerformed(evt);
            }
        });

        jLabel19.setText("%");

        erro.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        erro.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jButton6.setText("Salvar e Sair");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout Alterar_EspecificacoesLayout = new javax.swing.GroupLayout(Alterar_Especificacoes);
        Alterar_Especificacoes.setLayout(Alterar_EspecificacoesLayout);
        Alterar_EspecificacoesLayout.setHorizontalGroup(
            Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Alterar_EspecificacoesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(Alterar_EspecificacoesLayout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addGap(25, 25, 25)
                        .addComponent(diesel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel15))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, Alterar_EspecificacoesLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(preco_gasolina_atual))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, Alterar_EspecificacoesLayout.createSequentialGroup()
                        .addGroup(Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addGap(18, 18, 18)
                        .addGroup(Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(Alterar_EspecificacoesLayout.createSequentialGroup()
                                .addComponent(gasolina, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(preco_diesel_atual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(216, 216, 216)
                .addGroup(Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel16)
                    .addComponent(jLabel18))
                .addGap(20, 20, 20)
                .addGroup(Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(preco_alcool_atual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(Alterar_EspecificacoesLayout.createSequentialGroup()
                        .addComponent(alcool, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(Alterar_EspecificacoesLayout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(margem_atual, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Alterar_EspecificacoesLayout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(lucro, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(129, Short.MAX_VALUE))
            .addGroup(Alterar_EspecificacoesLayout.createSequentialGroup()
                .addGap(185, 185, 185)
                .addGroup(Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(erro, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(105, 105, 105)
                .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(28, 28, 28))
        );
        Alterar_EspecificacoesLayout.setVerticalGroup(
            Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Alterar_EspecificacoesLayout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(preco_gasolina_atual)
                        .addComponent(preco_alcool_atual, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(gasolina, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel9)
                    .addComponent(alcool, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(149, 149, 149)
                .addGroup(Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(preco_diesel_atual)
                    .addComponent(jLabel16)
                    .addComponent(margem_atual))
                .addGap(18, 18, 18)
                .addGroup(Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13)
                        .addComponent(diesel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel15))
                    .addGroup(Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel18)
                        .addComponent(lucro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel19)))
                .addGap(29, 29, 29)
                .addGroup(Alterar_EspecificacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(erro, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        Frete.setPreferredSize(new java.awt.Dimension(810, 505));

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Veiculo", "Distancia", "Tempo", "Carga", "Custo", "Frete", "Lucro"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        jLabel7.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        jLabel7.setText("Fretes Calculados:");

        jLabel10.setText("(Máximo = 10)");

        jLabel14.setFont(new java.awt.Font("Ubuntu", 0, 22)); // NOI18N
        jLabel14.setText("Lucro Total:");

        lucrotot.setFont(new java.awt.Font("Ubuntu", 0, 22)); // NOI18N
        lucrotot.setText("0.0 R$");

        jButton10.setText("Calcular Nova Entrega");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jButton11.setText("Apagar Todos os Fretes");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jButton15.setText("Voltar");
        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton15ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FreteLayout = new javax.swing.GroupLayout(Frete);
        Frete.setLayout(FreteLayout);
        FreteLayout.setHorizontalGroup(
            FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FreteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(FreteLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 786, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, FreteLayout.createSequentialGroup()
                        .addGroup(FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(FreteLayout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel10))
                            .addGroup(FreteLayout.createSequentialGroup()
                                .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34)
                                .addComponent(jButton11))
                            .addGroup(FreteLayout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lucrotot, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        FreteLayout.setVerticalGroup(
            FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FreteLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addGap(75, 75, 75)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lucrotot, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton15, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                .addContainerGap())
        );

        Calcula_Frete.setPreferredSize(new java.awt.Dimension(810, 505));

        jLabel20.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        jLabel20.setText("Calcular Entrega:");

        jLabel21.setText("Peso da Carga:");

        peso_texto.setText("0");
        peso_texto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                peso_textoActionPerformed(evt);
            }
        });

        jLabel22.setText("Kg");

        jLabel23.setText("Distância a ser percorrida:");

        jLabel24.setText("Km");

        distancia_texto.setText("0");
        distancia_texto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                distancia_textoActionPerformed(evt);
            }
        });

        jLabel25.setText("Tempo Máximo:");

        dias_texto.setText("0");
        dias_texto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dias_textoActionPerformed(evt);
            }
        });

        horas_texto.setText("0");

        jLabel26.setText("Dia(s)");

        jLabel27.setText("Hora(s)");

        jButton16.setText("Voltar");
        jButton16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton16ActionPerformed(evt);
            }
        });

        jButton17.setText("Calcular");
        jButton17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton17ActionPerformed(evt);
            }
        });

        erro2.setFont(new java.awt.Font("Ubuntu", 1, 17)); // NOI18N
        erro2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout Calcula_FreteLayout = new javax.swing.GroupLayout(Calcula_Frete);
        Calcula_Frete.setLayout(Calcula_FreteLayout);
        Calcula_FreteLayout.setHorizontalGroup(
            Calcula_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Calcula_FreteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Calcula_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(Calcula_FreteLayout.createSequentialGroup()
                        .addGroup(Calcula_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(Calcula_FreteLayout.createSequentialGroup()
                                .addComponent(jLabel25)
                                .addGap(18, 18, 18)
                                .addComponent(dias_texto, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel26)
                                .addGap(35, 35, 35)
                                .addComponent(horas_texto, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel27))
                            .addGroup(Calcula_FreteLayout.createSequentialGroup()
                                .addGroup(Calcula_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(Calcula_FreteLayout.createSequentialGroup()
                                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(peso_texto, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel22))
                                    .addGroup(Calcula_FreteLayout.createSequentialGroup()
                                        .addComponent(jLabel23)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(distancia_texto, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addComponent(jLabel24)))
                        .addContainerGap(466, Short.MAX_VALUE))
                    .addGroup(Calcula_FreteLayout.createSequentialGroup()
                        .addComponent(jButton17, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton16, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23))))
            .addGroup(Calcula_FreteLayout.createSequentialGroup()
                .addGap(155, 155, 155)
                .addComponent(erro2, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        Calcula_FreteLayout.setVerticalGroup(
            Calcula_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Calcula_FreteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(78, 78, 78)
                .addGroup(Calcula_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(peso_texto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22))
                .addGap(63, 63, 63)
                .addGroup(Calcula_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24)
                    .addComponent(distancia_texto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(63, 63, 63)
                .addGroup(Calcula_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dias_texto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(horas_texto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26)
                    .addComponent(jLabel27))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addComponent(erro2, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Calcula_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton16, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton17, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        Opcoes_Frete.setPreferredSize(new java.awt.Dimension(810, 505));

        jLabel28.setFont(new java.awt.Font("Ubuntu", 1, 20)); // NOI18N
        jLabel28.setText("Opções Viáveis:");

        jLabel29.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel29.setText("Menor Custo:");

        tipo_menor_custo.setText("Tipo:  Carreta");

        tempo_menor_custo.setText("Tempo:  xx dia(s)   yy hora(s)");

        custo_menor_custo.setText("Custo:   xxxx.xx R$");

        frete_menor_custo.setText("Frete a ser cobrado:  xxxx.xx R$");

        lucro_menor_custo.setText("Lucro: xxxx.xx R$");

        jButton18.setText("OK!");
        jButton18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton18ActionPerformed(evt);
            }
        });

        jLabel35.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel35.setText("Menor Tempo:      ");

        tipo_menor_tempo.setText("Tipo:  Carreta");

        tempo_menor_tempo.setText("Tempo:  xx dia(s)  yy hora(s)");

        custo_menor_tempo.setText("Custo:  xxxx.xx R$");

        frete_menor_tempo.setText("Frete a ser cobrado:  xxxx.xx R$");

        lucro_menor_tempo.setText("Lucro:  xxxx.xx R$");

        jButton23.setText("OK!");
        jButton23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton23ActionPerformed(evt);
            }
        });

        jLabel41.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel41.setText("Maior Lucro:");

        frete_maior_lucro.setText("Frete a ser cobrado:  xxxx.xx R$");

        tipo_maior_lucro.setText("Tipo: Carreta");

        tempo_maior_lucro.setText("Tempo:  xx dia(s)  yy hora(s)");

        custo_maior_lucro.setText("Custo:  xxxx.xx R$");

        lucro_maior_lucro.setText("Lucro:  xxxx.xx R$");

        jButton24.setText("OK!");
        jButton24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton24ActionPerformed(evt);
            }
        });

        jLabel47.setFont(new java.awt.Font("Ubuntu", 0, 17)); // NOI18N
        jLabel47.setText("Clique no botão abaixo da opção desejada!");

        jButton25.setText("Voltar sem selecionar nenhuma opção!");
        jButton25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton25ActionPerformed(evt);
            }
        });

        comb_maior_lucro.setText("Combustivel: Gasolina");

        comb_menor_custo.setText("Combustivel: Gasolina");

        comb_menor_tempo.setText("Combustível: Gasolina");

        javax.swing.GroupLayout Opcoes_FreteLayout = new javax.swing.GroupLayout(Opcoes_Frete);
        Opcoes_Frete.setLayout(Opcoes_FreteLayout);
        Opcoes_FreteLayout.setHorizontalGroup(
            Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Opcoes_FreteLayout.createSequentialGroup()
                .addGap(240, 240, 240)
                .addComponent(jLabel47)
                .addContainerGap(240, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Opcoes_FreteLayout.createSequentialGroup()
                .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Opcoes_FreteLayout.createSequentialGroup()
                        .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(Opcoes_FreteLayout.createSequentialGroup()
                                .addGap(329, 329, 329)
                                .addComponent(jLabel28))
                            .addGroup(Opcoes_FreteLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(tempo_menor_custo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
                                    .addComponent(tipo_menor_custo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(custo_menor_custo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(63, 63, 63)
                                .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tipo_maior_lucro)
                                    .addComponent(jLabel41)
                                    .addComponent(tempo_maior_lucro)
                                    .addComponent(custo_maior_lucro)))
                            .addGroup(Opcoes_FreteLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(Opcoes_FreteLayout.createSequentialGroup()
                                            .addComponent(jButton18, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(71, 71, 71))
                                        .addComponent(frete_menor_custo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(lucro_menor_custo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(comb_menor_custo))
                                .addGap(63, 63, 63)
                                .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lucro_maior_lucro)
                                    .addComponent(frete_maior_lucro)
                                    .addComponent(comb_maior_lucro)
                                    .addComponent(jButton24, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(Opcoes_FreteLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lucro_menor_tempo)
                            .addComponent(frete_menor_tempo)
                            .addComponent(jLabel35)
                            .addComponent(tipo_menor_tempo)
                            .addComponent(tempo_menor_tempo)
                            .addComponent(custo_menor_tempo)
                            .addComponent(jButton23, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comb_menor_tempo)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Opcoes_FreteLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton25)))
                .addContainerGap())
        );
        Opcoes_FreteLayout.setVerticalGroup(
            Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Opcoes_FreteLayout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)
                    .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel41)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tipo_menor_custo)
                    .addComponent(tipo_menor_tempo)
                    .addComponent(tipo_maior_lucro))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tempo_menor_custo)
                    .addComponent(tempo_menor_tempo)
                    .addComponent(tempo_maior_lucro))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(custo_menor_custo)
                    .addComponent(custo_menor_tempo)
                    .addComponent(custo_maior_lucro))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(frete_menor_custo)
                    .addComponent(frete_menor_tempo)
                    .addComponent(frete_maior_lucro))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lucro_menor_tempo)
                    .addComponent(lucro_menor_custo)
                    .addComponent(lucro_maior_lucro))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comb_maior_lucro)
                    .addComponent(comb_menor_custo)
                    .addComponent(comb_menor_tempo))
                .addGap(2, 2, 2)
                .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(Opcoes_FreteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton24, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton18, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(45, 45, 45)
                .addComponent(jLabel47)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addComponent(jButton25, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jLayeredPane1.setLayer(Menu, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(Gerenciador_de_Veiculos, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(Alterar_Especificacoes, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(Frete, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(Calcula_Frete, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(Opcoes_Frete, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 882, Short.MAX_VALUE)
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addGap(31, 31, 31)
                    .addComponent(Gerenciador_de_Veiculos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(32, Short.MAX_VALUE)))
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(Menu, javax.swing.GroupLayout.DEFAULT_SIZE, 858, Short.MAX_VALUE)
                    .addGap(12, 12, 12)))
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(Alterar_Especificacoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(Frete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(Calcula_Frete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(Opcoes_Frete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 600, Short.MAX_VALUE)
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(Gerenciador_de_Veiculos, javax.swing.GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE)
                    .addGap(12, 12, 12)))
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(Menu, javax.swing.GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE)
                    .addGap(12, 12, 12)))
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addComponent(Alterar_Especificacoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 95, Short.MAX_VALUE)))
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(Frete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(Calcula_Frete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLayeredPane1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(Opcoes_Frete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLayeredPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLayeredPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    public void escreve_arquivo() throws IOException {
        BufferedWriter buffWrite = new BufferedWriter(new FileWriter("data.txt", false));
        buffWrite.append(motos.size() + "\n");
        buffWrite.append(carros.size() + "\n");
        buffWrite.append(vans.size() + "\n");
        buffWrite.append(carretas.size() + "\n");
        buffWrite.append(preco_gasolina + "\n");
        buffWrite.append(preco_diesel + "\n");
        buffWrite.append(preco_alcool + "\n");
        buffWrite.append(margem_lucro + "\n");
        buffWrite.close();
    }
    
    public void le_arquivo()throws IOException {
        int tam_motos = Integer.parseInt(Files.readAllLines(Paths.get("data.txt")).get(0));
        int tam_carros = Integer.parseInt(Files.readAllLines(Paths.get("data.txt")).get(1));
        int tam_vans = Integer.parseInt(Files.readAllLines(Paths.get("data.txt")).get(2));
        int tam_carretas = Integer.parseInt(Files.readAllLines(Paths.get("data.txt")).get(3));
        for (int i = 0; i < tam_motos; i++) {
            motos.add(new Moto());
            motos_disponiveis.add(new Moto());
        }
        for (int i = 0; i < tam_carros; i++) {
            carros.add(new Carro());
            carros_disponiveis.add(new Carro());
        }
        for (int i = 0; i < tam_vans; i++) {
            vans.add(new Van());
            vans_disponiveis.add(new Van());
        }
        for (int i = 0; i < tam_carretas; i++) {
            carretas.add(new Carreta());
            carretas_disponiveis.add(new Carreta());
        }
        
        setPreco_gasolina(Float.parseFloat(Files.readAllLines(Paths.get("data.txt")).get(4)));
        setPreco_diesel(Float.parseFloat(Files.readAllLines(Paths.get("data.txt")).get(5)));
        setPreco_alcool(Float.parseFloat(Files.readAllLines(Paths.get("data.txt")).get(6)));
        setMargem_lucro(Float.parseFloat(Files.readAllLines(Paths.get("data.txt")).get(7)));
        resetatable();
    }
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        switchpanel(Gerenciador_de_Veiculos);    // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        motos.add(new Moto());
        motos_disponiveis.add(new Moto());
        resetatable();
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton21ActionPerformed
        carros.add(new Carro());
        carros_disponiveis.add(new Carro());
        resetatable();
    }//GEN-LAST:event_jButton21ActionPerformed

    private void jButton22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton22ActionPerformed
        vans.add(new Van());
        vans_disponiveis.add(new Van());
        resetatable();
    }//GEN-LAST:event_jButton22ActionPerformed

    private void jButton20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton20ActionPerformed
       carretas.add(new Carreta());
       carretas_disponiveis.add(new Carreta());
       resetatable();
    }//GEN-LAST:event_jButton20ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        if(motos.size()>0){
            Moto m = motos.get(0);
            motos.remove(m);
            if(motos_disponiveis.size()>0){
                m = motos_disponiveis.get(0);
                motos_disponiveis.remove(m);
            }
            resetatable();
        }
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        if(carros.size()>0){
            Carro c = carros.get(0);
            carros.remove(c);
            if(carros_disponiveis.size()>0){
                c = carros_disponiveis.get(0);
                carros_disponiveis.remove(c);
            }
            resetatable();
        }
    }//GEN-LAST:event_jButton13ActionPerformed

    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton14ActionPerformed
        if(vans.size()>0){
            Van v = vans.get(0);
            vans.remove(v);
            if(vans_disponiveis.size()>0){
                v = vans_disponiveis.get(0);
                vans_disponiveis.remove(v);
            }
            resetatable();
        }
    }//GEN-LAST:event_jButton14ActionPerformed

    private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton19ActionPerformed
       if(carretas.size()>0){
            Carreta c = carretas.get(0);
            carretas.remove(c);
            if(carretas_disponiveis.size()>0){
                c = carretas_disponiveis.get(0);
                carretas_disponiveis.remove(c);
            }
            resetatable();
        }
    }//GEN-LAST:event_jButton19ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        motos.clear();
        carros.clear();
        vans.clear();
        carretas.clear();
        fretes.clear();
        motos_disponiveis.clear();
        carros_disponiveis.clear();
        vans_disponiveis.clear();
        carretas_disponiveis.clear();
        
        resetatable();
        resetatable2();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        switchpanel(Menu);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        switchpanel(Alterar_Especificacoes);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void gasolinaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gasolinaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_gasolinaActionPerformed

    private void alcoolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_alcoolActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_alcoolActionPerformed

    private void dieselActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dieselActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dieselActionPerformed

    private void lucroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lucroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lucroActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        try{
            setPreco_gasolina(Float.parseFloat(gasolina.getText()));
            preco_gasolina_atual.setText("  "+ getPreco_gasolina() + "      R$");
            setPreco_alcool(Float.parseFloat(alcool.getText()));
            preco_alcool_atual.setText("  "+ getPreco_alcool() + "      R$");
            setPreco_diesel(Float.parseFloat(diesel.getText()));
            preco_diesel_atual.setText(getPreco_diesel() + "         R$");
            setMargem_lucro(Float.parseFloat(lucro.getText()));
            margem_atual.setText(getMargem_lucro() + " %");
            switchpanel(Menu);
        }
        catch(java.lang.NumberFormatException e){
            erro.setText("Entrada Inválida!");
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        switchpanel(Frete);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        switchpanel(Calcula_Frete);
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        for(Frete f: fretes){
            if(null != f.getVeiculo().getNome())switch (f.getVeiculo().getNome()) {
                case "Moto":
                    motos_disponiveis.add(new Moto());
                    break;
                case "Carro":
                    carros_disponiveis.add(new Carro());
                    break;
                case "Van":
                    vans_disponiveis.add(new Van());
                    break;
                case "Carreta":
                    carretas_disponiveis.add(new Carreta());
                    break;
                default:
                    break;
            }
        }
        fretes.clear();
        resetatable();
        resetatable2();
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton15ActionPerformed
        switchpanel(Menu);
    }//GEN-LAST:event_jButton15ActionPerformed

    private void peso_textoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_peso_textoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_peso_textoActionPerformed

    private void distancia_textoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_distancia_textoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_distancia_textoActionPerformed

    private void dias_textoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dias_textoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dias_textoActionPerformed

    private void jButton17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton17ActionPerformed
        try{
           int peso = 0;
           peso = Integer.parseInt(peso_texto.getText());
           int distancia = 0;
           distancia = Integer.parseInt(distancia_texto.getText());
           int tempo = 0;
           tempo = Integer.parseInt(dias_texto.getText())*24 + Integer.parseInt(horas_texto.getText());
           erro2.setText("");
           criafretes(peso,distancia,tempo);
        }
        catch(java.lang.NumberFormatException e){
            erro2.setText("Erro! Entrada(s) Inválidas.");
        }
    }//GEN-LAST:event_jButton17ActionPerformed

    private void jButton16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton16ActionPerformed
        switchpanel(Frete);
    }//GEN-LAST:event_jButton16ActionPerformed

    private void jButton23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton23ActionPerformed
        if(fretes.size()==10){
            if(null != fretes.get(0).getVeiculo().getNome())switch (fretes.get(0).getVeiculo().getNome()) {
                case "Moto":
                    motos_disponiveis.add(new Moto());
                    break;
                case "Carro":
                    carros_disponiveis.add(new Carro());
                    break;
                case "Van":
                    vans_disponiveis.add(new Van());
                    break;
                case "Carreta":
                    carretas_disponiveis.add(new Carreta());
                    break;
                default:
                    break;
            }
            fretes.remove(fretes.get(0));
        }
        fretes.add(menor_tempo);
        if("Moto".equals(menor_tempo.getVeiculo().getNome())){
            motos_disponiveis.remove(menor_tempo.getVeiculo());
        }
        else if("Carro".equals(menor_tempo.getVeiculo().getNome())){
            carros_disponiveis.remove(menor_tempo.getVeiculo());
        }
        else if("Van".equals(menor_tempo.getVeiculo().getNome())){
            vans_disponiveis.remove(menor_tempo.getVeiculo());
        }
        else if("Carreta".equals(menor_tempo.getVeiculo().getNome())){
            carretas_disponiveis.remove(menor_tempo.getVeiculo());
        }
        resetatable2();
        resetatable();
        switchpanel(Frete);
    }//GEN-LAST:event_jButton23ActionPerformed

    private void jButton24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton24ActionPerformed
        if(fretes.size()==10){
            if(null != fretes.get(0).getVeiculo().getNome())switch (fretes.get(0).getVeiculo().getNome()) {
                case "Moto":
                    motos_disponiveis.add(new Moto());
                    break;
                case "Carro":
                    carros_disponiveis.add(new Carro());
                    break;
                case "Van":
                    vans_disponiveis.add(new Van());
                    break;
                case "Carreta":
                    carretas_disponiveis.add(new Carreta());
                    break;
                default:
                    break;
            }
            fretes.remove(fretes.get(0));
        }
        fretes.add(maior_lucro);
        if("Moto".equals(maior_lucro.getVeiculo().getNome())){
            motos_disponiveis.remove(maior_lucro.getVeiculo());
        }
        else if("Carro".equals(maior_lucro.getVeiculo().getNome())){
            carros_disponiveis.remove(maior_lucro.getVeiculo());
        }
        else if("Van".equals(maior_lucro.getVeiculo().getNome())){
            vans_disponiveis.remove(maior_lucro.getVeiculo());
        }
        else if("Carreta".equals(maior_lucro.getVeiculo().getNome())){
            carretas_disponiveis.remove(maior_lucro.getVeiculo());
        }
        resetatable2();
        resetatable();
        switchpanel(Frete);
    }//GEN-LAST:event_jButton24ActionPerformed

    private void jButton25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton25ActionPerformed
        switchpanel(Calcula_Frete);
    }//GEN-LAST:event_jButton25ActionPerformed

    private void jButton18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton18ActionPerformed
        if(fretes.size()==10){
            if(null != fretes.get(0).getVeiculo().getNome())switch (fretes.get(0).getVeiculo().getNome()) {
                case "Moto":
                    motos_disponiveis.add(new Moto());
                    break;
                case "Carro":
                    carros_disponiveis.add(new Carro());
                    break;
                case "Van":
                    vans_disponiveis.add(new Van());
                    break;
                case "Carreta":
                    carretas_disponiveis.add(new Carreta());
                    break;
                default:
                    break;
            }
            fretes.remove(fretes.get(0));
        }
        fretes.add(menor_custo);
        if("Moto".equals(menor_custo.getVeiculo().getNome())){
            motos_disponiveis.remove(menor_custo.getVeiculo());
        }
        else if("Carro".equals(menor_custo.getVeiculo().getNome())){
            carros_disponiveis.remove(menor_custo.getVeiculo());
        }
        else if("Van".equals(menor_custo.getVeiculo().getNome())){
            vans_disponiveis.remove(menor_custo.getVeiculo());
        }
        else if("Carreta".equals(menor_custo.getVeiculo().getNome())){
            carretas_disponiveis.remove(menor_custo.getVeiculo());
        }
        resetatable2();
        resetatable();
        switchpanel(Frete);
    }//GEN-LAST:event_jButton18ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        try {
            escreve_arquivo();
            System.exit(0);
        } catch (IOException ex) {
            Logger.getLogger(Transportadora.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    public void inicialistas(){
        motos = new ArrayList<>();
        carros = new ArrayList<>();
        vans = new ArrayList<>();
        carretas = new ArrayList<>();
        motos_disponiveis = new ArrayList<>();
        carros_disponiveis = new ArrayList<>();
        vans_disponiveis = new ArrayList<>();
        carretas_disponiveis = new ArrayList<>();
        fretes = new ArrayList<>();
    }
    
    public void resetatable(){
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Moto", motos.size(), motos_disponiveis.size(), "50 kg", "43 Km/L - Alcool, 50 Km/L - Gasolina"},
                {"Carro", carros.size(), carros_disponiveis.size(), "360 kg", "12 Km/L - Álcool, 14Km/L - Gasolina"},
                {"Van", vans.size(), vans_disponiveis.size(), "3,5 ton", "10 Km/L - Diesel"},
                {"Carreta", carretas.size(),carretas_disponiveis.size(), "30 ton", "8 Km/L - Diesel"}
            },
            new String [] {
                "Tipo", "Qtde. Total", "Disponíveis", "Carga Máx", "Rendimento"
            }
        ));
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(80);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(80);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(80);
            jTable1.getColumnModel().getColumn(1).setMinWidth(100);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(1).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(2).setMinWidth(120);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(120);
            jTable1.getColumnModel().getColumn(2).setMaxWidth(120);
            jTable1.getColumnModel().getColumn(3).setMinWidth(100);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(3).setMaxWidth(100);
        }
    }
    
    public void resetatable2(){
        DecimalFormat numberformat = new DecimalFormat("0.00");
        if(fretes.size()==0){
            jTable2.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null}
                },
                new String [] {
                    "Veiculo", "Distancia", "Tempo", "Carga", "Custo", "Frete", "Lucro"
                }
            ));
        }
        if(fretes.size()==1){
            jTable2.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {fretes.get(0).getVeiculo().getNome(), fretes.get(0).getDistancia() + " Km", fretes.get(0).getTempo() + " Hora(s)", fretes.get(0).getCarga() + " Kg", numberformat.format(fretes.get(0).getCusto_operacao())+ " R$",numberformat.format(fretes.get(0).getFrete_final()) +" R$" , numberformat.format(fretes.get(0).getFrete_final()-fretes.get(0).getCusto_operacao()) + " R$"},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null}
                },
                new String [] {
                    "Veiculo", "Distancia", "Tempo", "Carga", "Custo", "Frete", "Lucro"
                }
            ));
        }
        if(fretes.size()==2){
            jTable2.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {fretes.get(0).getVeiculo().getNome(), fretes.get(0).getDistancia() + " Km", fretes.get(0).getTempo() + " Hora(s)", fretes.get(0).getCarga() + " Kg", numberformat.format(fretes.get(0).getCusto_operacao())+ " R$",numberformat.format(fretes.get(0).getFrete_final()) +" R$" , numberformat.format(fretes.get(0).getFrete_final()-fretes.get(0).getCusto_operacao())+ " R$"},
                    {fretes.get(1).getVeiculo().getNome(), fretes.get(1).getDistancia() + " Km", fretes.get(1).getTempo() + " Hora(s)", fretes.get(1).getCarga() + " Kg", numberformat.format(fretes.get(1).getCusto_operacao())+ " R$",numberformat.format(fretes.get(1).getFrete_final()) +" R$" , numberformat.format(fretes.get(1).getFrete_final()-fretes.get(1).getCusto_operacao())+ " R$"},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null}
                },
                new String [] {
                    "Veiculo", "Distancia", "Tempo", "Carga", "Custo", "Frete", "Lucro"
                }
            ));
        }
        if(fretes.size()==3){
            jTable2.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {fretes.get(0).getVeiculo().getNome(), fretes.get(0).getDistancia() + " Km", fretes.get(0).getTempo() + " Hora(s)", fretes.get(0).getCarga() + " Kg", numberformat.format(fretes.get(0).getCusto_operacao())+ " R$",numberformat.format(fretes.get(0).getFrete_final()) +" R$" , numberformat.format(fretes.get(0).getFrete_final()-fretes.get(0).getCusto_operacao())+ " R$"},
                    {fretes.get(1).getVeiculo().getNome(), fretes.get(1).getDistancia() + " Km", fretes.get(1).getTempo() + " Hora(s)", fretes.get(1).getCarga() + " Kg", numberformat.format(fretes.get(1).getCusto_operacao())+ " R$",numberformat.format(fretes.get(1).getFrete_final()) +" R$" , numberformat.format(fretes.get(1).getFrete_final()-fretes.get(1).getCusto_operacao())+ " R$"},
                    {fretes.get(2).getVeiculo().getNome(), fretes.get(2).getDistancia() + " Km", fretes.get(2).getTempo() + " Hora(s)", fretes.get(2).getCarga() + " Kg", numberformat.format(fretes.get(2).getCusto_operacao())+ " R$",numberformat.format(fretes.get(2).getFrete_final()) +" R$" , numberformat.format(fretes.get(2).getFrete_final()-fretes.get(2).getCusto_operacao())+ " R$"},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null}
                },
                new String [] {
                    "Veiculo", "Distancia", "Tempo", "Carga", "Custo", "Frete", "Lucro"
                }
            ));
        }
        if(fretes.size()==4){
            jTable2.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {fretes.get(0).getVeiculo().getNome(), fretes.get(0).getDistancia() + " Km", fretes.get(0).getTempo() + " Hora(s)", fretes.get(0).getCarga() + " Kg", numberformat.format(fretes.get(0).getCusto_operacao())+ " R$",numberformat.format(fretes.get(0).getFrete_final()) +" R$" , numberformat.format(fretes.get(0).getFrete_final()-fretes.get(0).getCusto_operacao())+ " R$"},
                    {fretes.get(1).getVeiculo().getNome(), fretes.get(1).getDistancia() + " Km", fretes.get(1).getTempo() + " Hora(s)", fretes.get(1).getCarga() + " Kg", numberformat.format(fretes.get(1).getCusto_operacao())+ " R$",numberformat.format(fretes.get(1).getFrete_final()) +" R$" , numberformat.format(fretes.get(1).getFrete_final()-fretes.get(1).getCusto_operacao())+ " R$"},
                    {fretes.get(2).getVeiculo().getNome(), fretes.get(2).getDistancia() + " Km", fretes.get(2).getTempo() + " Hora(s)", fretes.get(2).getCarga() + " Kg", numberformat.format(fretes.get(2).getCusto_operacao())+ " R$",numberformat.format(fretes.get(2).getFrete_final()) +" R$" , numberformat.format(fretes.get(2).getFrete_final()-fretes.get(2).getCusto_operacao())+ " R$"},
                    {fretes.get(3).getVeiculo().getNome(), fretes.get(3).getDistancia() + " Km", fretes.get(3).getTempo() + " Hora(s)", fretes.get(3).getCarga() + " Kg", numberformat.format(fretes.get(3).getCusto_operacao())+ " R$",numberformat.format(fretes.get(3).getFrete_final()) +" R$" , numberformat.format(fretes.get(3).getFrete_final()-fretes.get(3).getCusto_operacao())+ " R$"},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null}
                },
                new String [] {
                    "Veiculo", "Distancia", "Tempo", "Carga", "Custo", "Frete", "Lucro"
                }
            ));
        }
        if(fretes.size()==5){
            jTable2.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {fretes.get(0).getVeiculo().getNome(), fretes.get(0).getDistancia() + " Km", fretes.get(0).getTempo() + " Hora(s)", fretes.get(0).getCarga() + " Kg", numberformat.format(fretes.get(0).getCusto_operacao())+ " R$",numberformat.format(fretes.get(0).getFrete_final()) +" R$" , numberformat.format(fretes.get(0).getFrete_final()-fretes.get(0).getCusto_operacao())+ " R$"},
                    {fretes.get(1).getVeiculo().getNome(), fretes.get(1).getDistancia() + " Km", fretes.get(1).getTempo() + " Hora(s)", fretes.get(1).getCarga() + " Kg", numberformat.format(fretes.get(1).getCusto_operacao())+ " R$",numberformat.format(fretes.get(1).getFrete_final()) +" R$" , numberformat.format(fretes.get(1).getFrete_final()-fretes.get(1).getCusto_operacao())+ " R$"},
                    {fretes.get(2).getVeiculo().getNome(), fretes.get(2).getDistancia() + " Km", fretes.get(2).getTempo() + " Hora(s)", fretes.get(2).getCarga() + " Kg", numberformat.format(fretes.get(2).getCusto_operacao())+ " R$",numberformat.format(fretes.get(2).getFrete_final()) +" R$" , numberformat.format(fretes.get(2).getFrete_final()-fretes.get(2).getCusto_operacao())+ " R$"},
                    {fretes.get(3).getVeiculo().getNome(), fretes.get(3).getDistancia() + " Km", fretes.get(3).getTempo() + " Hora(s)", fretes.get(3).getCarga() + " Kg", numberformat.format(fretes.get(3).getCusto_operacao())+ " R$",numberformat.format(fretes.get(3).getFrete_final()) +" R$" , numberformat.format(fretes.get(3).getFrete_final()-fretes.get(3).getCusto_operacao())+ " R$"},
                    {fretes.get(4).getVeiculo().getNome(), fretes.get(4).getDistancia() + " Km", fretes.get(4).getTempo() + " Hora(s)", fretes.get(4).getCarga() + " Kg", numberformat.format(fretes.get(4).getCusto_operacao())+ " R$",numberformat.format(fretes.get(4).getFrete_final()) +" R$" , numberformat.format(fretes.get(4).getFrete_final()-fretes.get(4).getCusto_operacao())+ " R$"},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null}
                },
                new String [] {
                    "Veiculo", "Distancia", "Tempo", "Carga", "Custo", "Frete", "Lucro"
                }
            ));
        }
        if(fretes.size()==6){
            jTable2.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {fretes.get(0).getVeiculo().getNome(), fretes.get(0).getDistancia() + " Km", fretes.get(0).getTempo() + " Hora(s)", fretes.get(0).getCarga() + " Kg", numberformat.format(fretes.get(0).getCusto_operacao())+ " R$",numberformat.format(fretes.get(0).getFrete_final()) +" R$" , numberformat.format(fretes.get(0).getFrete_final()-fretes.get(0).getCusto_operacao())+ " R$"},
                    {fretes.get(1).getVeiculo().getNome(), fretes.get(1).getDistancia() + " Km", fretes.get(1).getTempo() + " Hora(s)", fretes.get(1).getCarga() + " Kg", numberformat.format(fretes.get(1).getCusto_operacao())+ " R$",numberformat.format(fretes.get(1).getFrete_final()) +" R$" , numberformat.format(fretes.get(1).getFrete_final()-fretes.get(1).getCusto_operacao())+ " R$"},
                    {fretes.get(2).getVeiculo().getNome(), fretes.get(2).getDistancia() + " Km", fretes.get(2).getTempo() + " Hora(s)", fretes.get(2).getCarga() + " Kg", numberformat.format(fretes.get(2).getCusto_operacao())+ " R$",numberformat.format(fretes.get(2).getFrete_final()) +" R$" , numberformat.format(fretes.get(2).getFrete_final()-fretes.get(2).getCusto_operacao())+ " R$"},
                    {fretes.get(3).getVeiculo().getNome(), fretes.get(3).getDistancia() + " Km", fretes.get(3).getTempo() + " Hora(s)", fretes.get(3).getCarga() + " Kg", numberformat.format(fretes.get(3).getCusto_operacao())+ " R$",numberformat.format(fretes.get(3).getFrete_final()) +" R$" , numberformat.format(fretes.get(3).getFrete_final()-fretes.get(3).getCusto_operacao())+ " R$"},
                    {fretes.get(4).getVeiculo().getNome(), fretes.get(4).getDistancia() + " Km", fretes.get(4).getTempo() + " Hora(s)", fretes.get(4).getCarga() + " Kg", numberformat.format(fretes.get(4).getCusto_operacao())+ " R$",numberformat.format(fretes.get(4).getFrete_final()) +" R$" , numberformat.format(fretes.get(4).getFrete_final()-fretes.get(4).getCusto_operacao())+ " R$"},
                    {fretes.get(5).getVeiculo().getNome(), fretes.get(5).getDistancia() + " Km", fretes.get(5).getTempo() + " Hora(s)", fretes.get(5).getCarga() + " Kg", numberformat.format(fretes.get(5).getCusto_operacao())+ " R$",numberformat.format(fretes.get(5).getFrete_final()) +" R$" , numberformat.format(fretes.get(5).getFrete_final()-fretes.get(5).getCusto_operacao())+ " R$"},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null}
                },
                new String [] {
                    "Veiculo", "Distancia", "Tempo", "Carga", "Custo", "Frete", "Lucro"
                }
            ));
        }
        if(fretes.size()==7){
            jTable2.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {fretes.get(0).getVeiculo().getNome(), fretes.get(0).getDistancia() + " Km", fretes.get(0).getTempo() + " Hora(s)", fretes.get(0).getCarga() + " Kg", numberformat.format(fretes.get(0).getCusto_operacao())+ " R$",numberformat.format(fretes.get(0).getFrete_final()) +" R$" , numberformat.format(fretes.get(0).getFrete_final()-fretes.get(0).getCusto_operacao())+ " R$"},
                    {fretes.get(1).getVeiculo().getNome(), fretes.get(1).getDistancia() + " Km", fretes.get(1).getTempo() + " Hora(s)", fretes.get(1).getCarga() + " Kg", numberformat.format(fretes.get(1).getCusto_operacao())+ " R$",numberformat.format(fretes.get(1).getFrete_final()) +" R$" , numberformat.format(fretes.get(1).getFrete_final()-fretes.get(1).getCusto_operacao())+ " R$"},
                    {fretes.get(2).getVeiculo().getNome(), fretes.get(2).getDistancia() + " Km", fretes.get(2).getTempo() + " Hora(s)", fretes.get(2).getCarga() + " Kg", numberformat.format(fretes.get(2).getCusto_operacao())+ " R$",numberformat.format(fretes.get(2).getFrete_final()) +" R$" , numberformat.format(fretes.get(2).getFrete_final()-fretes.get(2).getCusto_operacao())+ " R$"},
                    {fretes.get(3).getVeiculo().getNome(), fretes.get(3).getDistancia() + " Km", fretes.get(3).getTempo() + " Hora(s)", fretes.get(3).getCarga() + " Kg", numberformat.format(fretes.get(3).getCusto_operacao())+ " R$",numberformat.format(fretes.get(3).getFrete_final()) +" R$" , numberformat.format(fretes.get(3).getFrete_final()-fretes.get(3).getCusto_operacao())+ " R$"},
                    {fretes.get(4).getVeiculo().getNome(), fretes.get(4).getDistancia() + " Km", fretes.get(4).getTempo() + " Hora(s)", fretes.get(4).getCarga() + " Kg", numberformat.format(fretes.get(4).getCusto_operacao())+ " R$",numberformat.format(fretes.get(4).getFrete_final()) +" R$" , numberformat.format(fretes.get(4).getFrete_final()-fretes.get(4).getCusto_operacao())+ " R$"},
                    {fretes.get(5).getVeiculo().getNome(), fretes.get(5).getDistancia() + " Km", fretes.get(5).getTempo() + " Hora(s)", fretes.get(5).getCarga() + " Kg", numberformat.format(fretes.get(5).getCusto_operacao())+ " R$",numberformat.format(fretes.get(5).getFrete_final()) +" R$" , numberformat.format(fretes.get(5).getFrete_final()-fretes.get(5).getCusto_operacao())+ " R$"},
                    {fretes.get(6).getVeiculo().getNome(), fretes.get(6).getDistancia() + " Km", fretes.get(6).getTempo() + " Hora(s)", fretes.get(6).getCarga() + " Kg", numberformat.format(fretes.get(6).getCusto_operacao())+ " R$",numberformat.format(fretes.get(6).getFrete_final()) +" R$" , numberformat.format(fretes.get(6).getFrete_final()-fretes.get(6).getCusto_operacao())+ " R$"},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null}
                },
                new String [] {
                    "Veiculo", "Distancia", "Tempo", "Carga", "Custo", "Frete", "Lucro"
                }
            ));
        }
        if(fretes.size()==8){
            jTable2.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {fretes.get(0).getVeiculo().getNome(), fretes.get(0).getDistancia() + " Km", fretes.get(0).getTempo() + " Hora(s)", fretes.get(0).getCarga() + " Kg", numberformat.format(fretes.get(0).getCusto_operacao())+ " R$",numberformat.format(fretes.get(0).getFrete_final()) +" R$" , numberformat.format(fretes.get(0).getFrete_final()-fretes.get(0).getCusto_operacao())+ " R$"},
                    {fretes.get(1).getVeiculo().getNome(), fretes.get(1).getDistancia() + " Km", fretes.get(1).getTempo() + " Hora(s)", fretes.get(1).getCarga() + " Kg", numberformat.format(fretes.get(1).getCusto_operacao())+ " R$",numberformat.format(fretes.get(1).getFrete_final()) +" R$" , numberformat.format(fretes.get(1).getFrete_final()-fretes.get(1).getCusto_operacao())+ " R$"},
                    {fretes.get(2).getVeiculo().getNome(), fretes.get(2).getDistancia() + " Km", fretes.get(2).getTempo() + " Hora(s)", fretes.get(2).getCarga() + " Kg", numberformat.format(fretes.get(2).getCusto_operacao())+ " R$",numberformat.format(fretes.get(2).getFrete_final()) +" R$" , numberformat.format(fretes.get(2).getFrete_final()-fretes.get(2).getCusto_operacao())+ " R$"},
                    {fretes.get(3).getVeiculo().getNome(), fretes.get(3).getDistancia() + " Km", fretes.get(3).getTempo() + " Hora(s)", fretes.get(3).getCarga() + " Kg", numberformat.format(fretes.get(3).getCusto_operacao())+ " R$",numberformat.format(fretes.get(3).getFrete_final()) +" R$" , numberformat.format(fretes.get(3).getFrete_final()-fretes.get(3).getCusto_operacao())+ " R$"},
                    {fretes.get(4).getVeiculo().getNome(), fretes.get(4).getDistancia() + " Km", fretes.get(4).getTempo() + " Hora(s)", fretes.get(4).getCarga() + " Kg", numberformat.format(fretes.get(4).getCusto_operacao())+ " R$",numberformat.format(fretes.get(4).getFrete_final()) +" R$" , numberformat.format(fretes.get(4).getFrete_final()-fretes.get(4).getCusto_operacao())+ " R$"},
                    {fretes.get(5).getVeiculo().getNome(), fretes.get(5).getDistancia() + " Km", fretes.get(5).getTempo() + " Hora(s)", fretes.get(5).getCarga() + " Kg", numberformat.format(fretes.get(5).getCusto_operacao())+ " R$",numberformat.format(fretes.get(5).getFrete_final()) +" R$" , numberformat.format(fretes.get(5).getFrete_final()-fretes.get(5).getCusto_operacao())+ " R$"},
                    {fretes.get(6).getVeiculo().getNome(), fretes.get(6).getDistancia() + " Km", fretes.get(6).getTempo() + " Hora(s)", fretes.get(6).getCarga() + " Kg", numberformat.format(fretes.get(6).getCusto_operacao())+ " R$",numberformat.format(fretes.get(6).getFrete_final()) +" R$" , numberformat.format(fretes.get(6).getFrete_final()-fretes.get(6).getCusto_operacao())+ " R$"},
                    {fretes.get(7).getVeiculo().getNome(), fretes.get(7).getDistancia() + " Km", fretes.get(7).getTempo() + " Hora(s)", fretes.get(7).getCarga() + " Kg", numberformat.format(fretes.get(7).getCusto_operacao())+ " R$",numberformat.format(fretes.get(7).getFrete_final()) +" R$" , numberformat.format(fretes.get(7).getFrete_final()-fretes.get(7).getCusto_operacao())+ " R$"},
                    {null, null, null, null, null, null, null},
                    {null, null, null, null, null, null, null}
                },
                new String [] {
                    "Veiculo", "Distancia", "Tempo", "Carga", "Custo", "Frete", "Lucro"
                }
            ));
        }
        if(fretes.size()==9){
            jTable2.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {fretes.get(0).getVeiculo().getNome(), fretes.get(0).getDistancia() + " Km", fretes.get(0).getTempo() + " Hora(s)", fretes.get(0).getCarga() + " Kg", numberformat.format(fretes.get(0).getCusto_operacao())+ " R$",numberformat.format(fretes.get(0).getFrete_final()) +" R$" , numberformat.format(fretes.get(0).getFrete_final()-fretes.get(0).getCusto_operacao())+ " R$"},
                    {fretes.get(1).getVeiculo().getNome(), fretes.get(1).getDistancia() + " Km", fretes.get(1).getTempo() + " Hora(s)", fretes.get(1).getCarga() + " Kg", numberformat.format(fretes.get(1).getCusto_operacao())+ " R$",numberformat.format(fretes.get(1).getFrete_final()) +" R$" , numberformat.format(fretes.get(1).getFrete_final()-fretes.get(1).getCusto_operacao())+ " R$"},
                    {fretes.get(2).getVeiculo().getNome(), fretes.get(2).getDistancia() + " Km", fretes.get(2).getTempo() + " Hora(s)", fretes.get(2).getCarga() + " Kg", numberformat.format(fretes.get(2).getCusto_operacao())+ " R$",numberformat.format(fretes.get(2).getFrete_final()) +" R$" , numberformat.format(fretes.get(2).getFrete_final()-fretes.get(2).getCusto_operacao())+ " R$"},
                    {fretes.get(3).getVeiculo().getNome(), fretes.get(3).getDistancia() + " Km", fretes.get(3).getTempo() + " Hora(s)", fretes.get(3).getCarga() + " Kg", numberformat.format(fretes.get(3).getCusto_operacao())+ " R$",numberformat.format(fretes.get(3).getFrete_final()) +" R$" , numberformat.format(fretes.get(3).getFrete_final()-fretes.get(3).getCusto_operacao())+ " R$"},
                    {fretes.get(4).getVeiculo().getNome(), fretes.get(4).getDistancia() + " Km", fretes.get(4).getTempo() + " Hora(s)", fretes.get(4).getCarga() + " Kg", numberformat.format(fretes.get(4).getCusto_operacao())+ " R$",numberformat.format(fretes.get(4).getFrete_final()) +" R$" , numberformat.format(fretes.get(4).getFrete_final()-fretes.get(4).getCusto_operacao())+ " R$"},
                    {fretes.get(5).getVeiculo().getNome(), fretes.get(5).getDistancia() + " Km", fretes.get(5).getTempo() + " Hora(s)", fretes.get(5).getCarga() + " Kg", numberformat.format(fretes.get(5).getCusto_operacao())+ " R$",numberformat.format(fretes.get(5).getFrete_final()) +" R$" , numberformat.format(fretes.get(5).getFrete_final()-fretes.get(5).getCusto_operacao())+ " R$"},
                    {fretes.get(6).getVeiculo().getNome(), fretes.get(6).getDistancia() + " Km", fretes.get(6).getTempo() + " Hora(s)", fretes.get(6).getCarga() + " Kg", numberformat.format(fretes.get(6).getCusto_operacao())+ " R$",numberformat.format(fretes.get(6).getFrete_final()) +" R$" , numberformat.format(fretes.get(6).getFrete_final()-fretes.get(6).getCusto_operacao())+ " R$"},
                    {fretes.get(7).getVeiculo().getNome(), fretes.get(7).getDistancia() + " Km", fretes.get(7).getTempo() + " Hora(s)", fretes.get(7).getCarga() + " Kg", numberformat.format(fretes.get(7).getCusto_operacao())+ " R$",numberformat.format(fretes.get(7).getFrete_final()) +" R$" , numberformat.format(fretes.get(7).getFrete_final()-fretes.get(7).getCusto_operacao())+ " R$"},
                    {fretes.get(8).getVeiculo().getNome(), fretes.get(8).getDistancia() + " Km", fretes.get(8).getTempo() + " Hora(s)", fretes.get(8).getCarga() + " Kg", numberformat.format(fretes.get(8).getCusto_operacao())+ " R$",numberformat.format(fretes.get(8).getFrete_final()) +" R$" , numberformat.format(fretes.get(8).getFrete_final()-fretes.get(8).getCusto_operacao())+ " R$"},
                    {null, null, null, null, null, null, null}
                },
                new String [] {
                    "Veiculo", "Distancia", "Tempo", "Carga", "Custo", "Frete", "Lucro"
                }
            ));
        }
        if(fretes.size()==10){
            jTable2.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {fretes.get(0).getVeiculo().getNome(), fretes.get(0).getDistancia() + " Km", fretes.get(0).getTempo() + " Hora(s)", fretes.get(0).getCarga() + " Kg", numberformat.format(fretes.get(0).getCusto_operacao())+ " R$",numberformat.format(fretes.get(0).getFrete_final()) +" R$" , numberformat.format(fretes.get(0).getFrete_final()-fretes.get(0).getCusto_operacao())+ " R$"},
                    {fretes.get(1).getVeiculo().getNome(), fretes.get(1).getDistancia() + " Km", fretes.get(1).getTempo() + " Hora(s)", fretes.get(1).getCarga() + " Kg", numberformat.format(fretes.get(1).getCusto_operacao())+ " R$",numberformat.format(fretes.get(1).getFrete_final()) +" R$" , numberformat.format(fretes.get(1).getFrete_final()-fretes.get(1).getCusto_operacao())+ " R$"},
                    {fretes.get(2).getVeiculo().getNome(), fretes.get(2).getDistancia() + " Km", fretes.get(2).getTempo() + " Hora(s)", fretes.get(2).getCarga() + " Kg", numberformat.format(fretes.get(2).getCusto_operacao())+ " R$",numberformat.format(fretes.get(2).getFrete_final()) +" R$" , numberformat.format(fretes.get(2).getFrete_final()-fretes.get(2).getCusto_operacao())+ " R$"},
                    {fretes.get(3).getVeiculo().getNome(), fretes.get(3).getDistancia() + " Km", fretes.get(3).getTempo() + " Hora(s)", fretes.get(3).getCarga() + " Kg", numberformat.format(fretes.get(3).getCusto_operacao())+ " R$",numberformat.format(fretes.get(3).getFrete_final()) +" R$" , numberformat.format(fretes.get(3).getFrete_final()-fretes.get(3).getCusto_operacao())+ " R$"},
                    {fretes.get(4).getVeiculo().getNome(), fretes.get(4).getDistancia() + " Km", fretes.get(4).getTempo() + " Hora(s)", fretes.get(4).getCarga() + " Kg", numberformat.format(fretes.get(4).getCusto_operacao())+ " R$",numberformat.format(fretes.get(4).getFrete_final()) +" R$" , numberformat.format(fretes.get(4).getFrete_final()-fretes.get(4).getCusto_operacao())+ " R$"},
                    {fretes.get(5).getVeiculo().getNome(), fretes.get(5).getDistancia() + " Km", fretes.get(5).getTempo() + " Hora(s)", fretes.get(5).getCarga() + " Kg", numberformat.format(fretes.get(5).getCusto_operacao())+ " R$",numberformat.format(fretes.get(5).getFrete_final()) +" R$" , numberformat.format(fretes.get(5).getFrete_final()-fretes.get(5).getCusto_operacao())+ " R$"},
                    {fretes.get(6).getVeiculo().getNome(), fretes.get(6).getDistancia() + " Km", fretes.get(6).getTempo() + " Hora(s)", fretes.get(6).getCarga() + " Kg", numberformat.format(fretes.get(6).getCusto_operacao())+ " R$",numberformat.format(fretes.get(6).getFrete_final()) +" R$" , numberformat.format(fretes.get(6).getFrete_final()-fretes.get(6).getCusto_operacao())+ " R$"},
                    {fretes.get(7).getVeiculo().getNome(), fretes.get(7).getDistancia() + " Km", fretes.get(7).getTempo() + " Hora(s)", fretes.get(7).getCarga() + " Kg", numberformat.format(fretes.get(7).getCusto_operacao())+ " R$",numberformat.format(fretes.get(7).getFrete_final()) +" R$" , numberformat.format(fretes.get(7).getFrete_final()-fretes.get(7).getCusto_operacao())+ " R$"},
                    {fretes.get(8).getVeiculo().getNome(), fretes.get(8).getDistancia() + " Km", fretes.get(8).getTempo() + " Hora(s)", fretes.get(8).getCarga() + " Kg", numberformat.format(fretes.get(8).getCusto_operacao())+ " R$",numberformat.format(fretes.get(8).getFrete_final()) +" R$" , numberformat.format(fretes.get(8).getFrete_final()-fretes.get(8).getCusto_operacao())+ " R$"},
                    {fretes.get(9).getVeiculo().getNome(), fretes.get(9).getDistancia() + " Km", fretes.get(9).getTempo() + " Hora(s)", fretes.get(9).getCarga() + " Kg", numberformat.format(fretes.get(9).getCusto_operacao())+ " R$",numberformat.format(fretes.get(9).getFrete_final()) +" R$" , numberformat.format(fretes.get(9).getFrete_final()-fretes.get(9).getCusto_operacao())+ " R$"},
                },
                new String [] {
                    "Veiculo", "Distancia", "Tempo", "Carga", "Custo", "Frete", "Lucro"
                }
            ));
        }
        double lucrototal= 0;
        for(Frete f: fretes){
            lucrototal = lucrototal + f.getFrete_final() - f.getCusto_operacao();
        }
        lucrotot.setText(numberformat.format(lucrototal)+ " R$");
    }
    
    public void criafretes(int peso, int distancia, int tempo){
        ArrayList<Frete> fretes_possiveis = new ArrayList<>();
        if(motos_disponiveis.size()>0){
            double preco_moto = motos_disponiveis.get(0).calcula_preco(distancia, peso, preco_alcool, preco_gasolina);
            int tempo_moto = motos_disponiveis.get(0).calcula_tempo(distancia);
            if(preco_moto>0 && tempo_moto< tempo){
                double frete_moto = preco_moto/(1-(getMargem_lucro()*0.01));
                fretes_possiveis.add(new Frete(frete_moto, preco_moto, tempo_moto, motos_disponiveis.get(0),distancia,peso));
            }
        }
        if(carros_disponiveis.size()>0){
            double preco_carro = carros_disponiveis.get(0).calcula_preco(distancia, peso, preco_alcool, preco_gasolina);
            int tempo_carro = carros_disponiveis.get(0).calcula_tempo(distancia);
            if(preco_carro>0 && tempo_carro< tempo){
                double frete_carro = preco_carro/(1-(getMargem_lucro()*0.01));
                fretes_possiveis.add(new Frete(frete_carro, preco_carro, tempo_carro, carros_disponiveis.get(0),distancia,peso));
            }
        }
        if(carretas_disponiveis.size()>0){
            double preco_carreta = carretas_disponiveis.get(0).calcula_preco(distancia, peso, preco_diesel);
            int tempo_carreta = carretas_disponiveis.get(0).calcula_tempo(distancia);
            if(preco_carreta>0 && tempo_carreta< tempo){
                double frete_carreta = preco_carreta/(1-(getMargem_lucro()*0.01));
                fretes_possiveis.add(new Frete(frete_carreta, preco_carreta, tempo_carreta, carretas_disponiveis.get(0),distancia,peso));
            }
        }
        if(vans_disponiveis.size()>0){
            double preco_van = vans_disponiveis.get(0).calcula_preco(distancia, peso, preco_diesel);
            int tempo_van = vans_disponiveis.get(0).calcula_tempo(distancia);
            if(preco_van>0 && tempo_van< tempo){
                double frete_moto = preco_van/(1-(getMargem_lucro()*0.01));
                fretes_possiveis.add(new Frete(frete_moto, preco_van, tempo_van, vans_disponiveis.get(0),distancia,peso));
            }
        }
        if(fretes_possiveis.size()>0){
            menor_custo = fretes_possiveis.get(0);
            menor_tempo = fretes_possiveis.get(0);
            maior_lucro = fretes_possiveis.get(0);
            for (Frete f: fretes_possiveis) {
                if(f.getCusto_operacao()>menor_custo.getCusto_operacao()){
                    menor_custo = f;
                }
                if(f.getTempo()< menor_tempo.getTempo()){
                    menor_tempo = f;
                }
                if((f.getFrete_final()-f.getCusto_operacao()) > maior_lucro.getFrete_final()-maior_lucro.getCusto_operacao()){
                    maior_lucro = f;
                }
            }
            tipo_menor_custo.setText("Tipo:  "+ menor_custo.getVeiculo().getNome());
            int dias = menor_custo.getTempo()/(24);
            int horas = (menor_custo.getTempo())-dias*24;
            tempo_menor_custo.setText("Tempo:  "+ dias + " dia(s)  "+ horas + " hora(s)" );
            DecimalFormat numberformat = new DecimalFormat("0.00");
            custo_menor_custo.setText("Custo:  "+numberformat.format(menor_custo.getCusto_operacao()));
            frete_menor_custo.setText("Frete a ser cobrado:  "+numberformat.format(menor_custo.getFrete_final()));
            lucro_menor_custo.setText("Lucro:  "+ (numberformat.format(menor_custo.getFrete_final()-menor_custo.getCusto_operacao())));
            comb_menor_custo.setText("Combustível: "+ menor_custo.getVeiculo().getCombustivel());
            
            tipo_maior_lucro.setText("Tipo:  "+ maior_lucro.getVeiculo().getNome());
            dias = maior_lucro.getTempo()/(24);
            horas = (maior_lucro.getTempo())-dias*24;
            tempo_maior_lucro.setText("Tempo:  "+ dias + " dia(s)  "+ horas + " hora(s)" );
            custo_maior_lucro.setText("Custo:  "+numberformat.format(maior_lucro.getCusto_operacao()));
            frete_maior_lucro.setText("Frete a ser cobrado:  "+numberformat.format(maior_lucro.getFrete_final()));
            lucro_maior_lucro.setText("Lucro:  "+ (numberformat.format(maior_lucro.getFrete_final()-maior_lucro.getCusto_operacao())));
            comb_maior_lucro.setText("Combustível: "+ maior_lucro.getVeiculo().getCombustivel());
            
            tipo_menor_tempo.setText("Tipo:  "+ menor_tempo.getVeiculo().getNome());
            dias = menor_tempo.getTempo()/(24);
            horas = (menor_tempo.getTempo())-dias*24;
            tempo_menor_tempo.setText("Tempo:  "+ dias + " dia(s)  "+ horas + " hora(s)" );
            custo_menor_tempo.setText("Custo:  "+numberformat.format(menor_tempo.getCusto_operacao()));
            frete_menor_tempo.setText("Frete a ser cobrado:  "+numberformat.format(menor_tempo.getFrete_final()));
            lucro_menor_tempo.setText("Lucro:  "+ (numberformat.format(menor_tempo.getFrete_final()-menor_tempo.getCusto_operacao())));
            comb_menor_tempo.setText("Combustível: "+ menor_tempo.getVeiculo().getCombustivel());

            switchpanel(Opcoes_Frete);
            
        }
        else{
            erro2.setText("Não há veículos disponíveis que possam realizar a entrega");
        }
    }
    
    /**
     * @param args the command line arguments
     */
    
    
    public void switchpanel(JPanel panel){
        jLayeredPane1.removeAll();
        jLayeredPane1.add(panel);
        jLayeredPane1.repaint();
        jLayeredPane1.revalidate();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Alterar_Especificacoes;
    private javax.swing.JPanel Calcula_Frete;
    private javax.swing.JPanel Frete;
    private javax.swing.JPanel Gerenciador_de_Veiculos;
    private javax.swing.JPanel Menu;
    private javax.swing.JPanel Opcoes_Frete;
    private javax.swing.JTextField alcool;
    private javax.swing.JLabel comb_maior_lucro;
    private javax.swing.JLabel comb_menor_custo;
    private javax.swing.JLabel comb_menor_tempo;
    private javax.swing.JLabel custo_maior_lucro;
    private javax.swing.JLabel custo_menor_custo;
    private javax.swing.JLabel custo_menor_tempo;
    private javax.swing.JTextField dias_texto;
    private javax.swing.JTextField diesel;
    private javax.swing.JTextField distancia_texto;
    private javax.swing.JLabel erro;
    private javax.swing.JLabel erro2;
    private javax.swing.JLabel frete_maior_lucro;
    private javax.swing.JLabel frete_menor_custo;
    private javax.swing.JLabel frete_menor_tempo;
    private javax.swing.JTextField gasolina;
    private javax.swing.JTextField horas_texto;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton17;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton20;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton22;
    private javax.swing.JButton jButton23;
    private javax.swing.JButton jButton24;
    private javax.swing.JButton jButton25;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTextField lucro;
    private javax.swing.JLabel lucro_maior_lucro;
    private javax.swing.JLabel lucro_menor_custo;
    private javax.swing.JLabel lucro_menor_tempo;
    private javax.swing.JLabel lucrotot;
    private javax.swing.JLabel margem_atual;
    private javax.swing.JTextField peso_texto;
    private javax.swing.JLabel preco_alcool_atual;
    private javax.swing.JLabel preco_diesel_atual;
    private javax.swing.JLabel preco_gasolina_atual;
    private javax.swing.JLabel tempo_maior_lucro;
    private javax.swing.JLabel tempo_menor_custo;
    private javax.swing.JLabel tempo_menor_tempo;
    private javax.swing.JLabel tipo_maior_lucro;
    private javax.swing.JLabel tipo_menor_custo;
    private javax.swing.JLabel tipo_menor_tempo;
    // End of variables declaration//GEN-END:variables
    public ArrayList<Moto> motos;
    public ArrayList<Carro> carros;
    public ArrayList<Van> vans;
    public ArrayList<Carreta> carretas;
    public ArrayList<Moto> motos_disponiveis;
    public ArrayList<Carro> carros_disponiveis;
    public ArrayList<Van> vans_disponiveis;
    public ArrayList<Carreta> carretas_disponiveis;
    public ArrayList<Frete> fretes;
    private float preco_diesel;
    private float preco_gasolina;
    private float preco_alcool;
    private float margem_lucro;
    private Frete menor_custo;
    private Frete menor_tempo;
    private Frete maior_lucro;

    public float getPreco_diesel() {
        return preco_diesel;
    }

    public void setPreco_diesel(float preco_diesel) {
        preco_diesel_atual.setText(preco_diesel + "         R$");
        diesel.setText(String.valueOf(preco_diesel));
        this.preco_diesel = preco_diesel;
    }

    public float getPreco_gasolina() {
        return preco_gasolina;
    }

    public void setPreco_gasolina(float preco_gasolina) {
        preco_gasolina_atual.setText("  "+ preco_gasolina + "      R$");
        gasolina.setText(String.valueOf(preco_gasolina));
        this.preco_gasolina = preco_gasolina;
    }

    public float getPreco_alcool() {
        return preco_alcool;
    }

    public void setPreco_alcool(float preco_alcool) {
        preco_alcool_atual.setText("  "+ preco_alcool + "      R$");
        alcool.setText(String.valueOf(preco_alcool));
        this.preco_alcool = preco_alcool;
    }

    public float getMargem_lucro() {
        return margem_lucro;
    }

    public void setMargem_lucro(float margem_lucro) {
        margem_atual.setText(margem_lucro + " %");
        lucro.setText(String.valueOf(margem_lucro));
        this.margem_lucro = margem_lucro;
    }
}
