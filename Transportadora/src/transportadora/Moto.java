/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transportadora;

/**
 *
 * @author murilo
 */
public class Moto extends Veiculo {
    
    public Moto(){
        this.setVel_media(110);
        this.setCarga_maxima(50);
        this.setCombustivel("Gasolina ou Alcool");
        this.setRendimento(0);
        this.setNome("Moto");
    }
    
    public double calcula_preco(int distancia, int peso, float preco_alcool, float preco_gasolina) {
        if(peso<this.getCarga_maxima()){
            double custo_alcool;
            double custo_gasolina;
            this.setRendimento(43-(0.4*peso));
            custo_alcool = (distancia/this.getRendimento())*preco_alcool;
            this.setRendimento(50-(0.3*peso));
            custo_gasolina = (distancia/this.getRendimento())*preco_gasolina;
            if(custo_alcool<custo_gasolina){
                this.setCombustivel("Alcool");
                return custo_alcool;
            }
            else if(custo_gasolina<custo_alcool){
                this.setCombustivel("Gasolina");
                return custo_gasolina;
            }
            else{
                return custo_alcool;
            }
        }
        else
            return -1;
    }
    
    
}
