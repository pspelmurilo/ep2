/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transportadora;

/**
 *
 * @author murilo
 */
public class Van extends Veiculo {
    
    public Van(){
        this.setVel_media(80);
        this.setCarga_maxima(3500);
        this.setCombustivel("Diesel");
        this.setRendimento(10);
        this.setNome("Van");
    }
    
    public double calcula_preco(int distancia, int peso, float preco_diesel) {
        if(peso<this.getCarga_maxima()){
            this.setRendimento(this.getRendimento()-(0.001*peso));
            return (distancia/this.getRendimento())*preco_diesel;
        }
        else
            return 0;
    }
    
    
}
