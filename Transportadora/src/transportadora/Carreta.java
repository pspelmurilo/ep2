/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transportadora;

/**
 *
 * @author murilo
 */
public class Carreta extends Veiculo {
    public Carreta(){
        this.setVel_media(60);
        this.setCarga_maxima(30000);
        this.setCombustivel("Diesel");
        this.setRendimento(8);
        this.setNome("Carreta");
    }
    
    public double calcula_preco(int distancia, int peso,float preco_diesel) {
        if(peso<this.getCarga_maxima()){
            this.setRendimento(this.getRendimento()-(0.0002*peso));
            return (distancia/this.getRendimento())*preco_diesel;
        }
        else
            return 0;
    }
    
}
