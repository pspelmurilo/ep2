/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transportadora;

/**
 *
 * @author murilo
 */
public class Frete {
    private double frete_final;
    private double custo_operacao;
    private int tempo;
    private Veiculo veiculo;
    private int distancia;
    private int carga;

    public Frete(double frete_final, double custo_operacao, int tempo, Veiculo veiculo, int distancia, int carga) {
        this.frete_final = frete_final;
        this.custo_operacao = custo_operacao;
        this.tempo = tempo;
        this.veiculo = veiculo;
        this.distancia = distancia;
        this.carga = carga;
    }

    public int getDistancia() {
        return distancia;
    }

    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }

    public int getCarga() {
        return carga;
    }

    public void setCarga(int carga) {
        this.carga = carga;
    }

    

    public int getTempo() {
        return tempo;
    }

    public void setTempo(int tempo) {
        this.tempo = tempo;
    }
    public double getFrete_final() {
        return frete_final;
    }

    public void setFrete_final(double frete_final) {
        this.frete_final = frete_final;
    }

    public double getCusto_operacao() {
        return custo_operacao;
    }

    public void setCusto_operacao(double custo_operacao) {
        this.custo_operacao = custo_operacao;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }
}
