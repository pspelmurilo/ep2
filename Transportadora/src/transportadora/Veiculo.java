/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transportadora;

/**
 *
 * @author murilo
 */
public class Veiculo {

    private String nome;

    private String combustivel;
    private double rendimento;
    private int carga_maxima;
    private int vel_media;

    public Veiculo() {
        this.vel_media = 0;
        this.carga_maxima = 0;
        this.combustivel = "";
        this.rendimento = 0;
        this.nome = "";
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }

    public double getRendimento() {
        return rendimento;
    }

    public void setRendimento(double rendimento) {
        this.rendimento = rendimento;
    }

    public int getCarga_maxima() {
        return carga_maxima;
    }

    public void setCarga_maxima(int carga_maxima) {
        this.carga_maxima = carga_maxima;
    }

    public int getVel_media() {
        return vel_media;
    }

    public void setVel_media(int vel_media) {
        this.vel_media = vel_media;
    }

    public double calcula_preco(int distancia, int peso) {
        return 0;
    }

    public int calcula_tempo(int distancia) {
        return (distancia/this.getVel_media());
    }
    

}
