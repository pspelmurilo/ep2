/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transportadora;

/**
 *
 * @author murilo
 */
public class Carro  extends Veiculo{
    public Carro(){
        this.setVel_media(100);
        this.setCarga_maxima(360);
        this.setCombustivel("Gasolina ou Alcool");
        this.setRendimento(0);
        this.setNome("Carro");
    }
    
    public double calcula_preco(int distancia, int peso, float preco_alcool, float preco_gasolina) {
        if(peso<this.getCarga_maxima()){
            double custo_alcool;
            double custo_gasolina;
            this.setRendimento(12-(0.0231*peso));
            custo_alcool = (distancia/this.getRendimento())*preco_alcool;
            this.setRendimento(14-(0.025*peso));
            custo_gasolina = (distancia/this.getRendimento())*preco_gasolina;
            if(preco_alcool<preco_gasolina){
                this.setCombustivel("Alcool");
                return custo_alcool;
            }
            else if(preco_gasolina<preco_alcool){
                this.setCombustivel("Gasolina");
                return custo_gasolina;
            }
            else{
                return custo_alcool;
            }
        }
        else
            return 0;
    }
    
}
